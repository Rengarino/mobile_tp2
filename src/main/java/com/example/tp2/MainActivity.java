package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public ListView listWine;
    public SimpleCursorAdapter adapter2;
    public WineDbHelper WDH;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        listWine= (ListView) findViewById(R.id.listWine);
        WDH = new WineDbHelper(this);
        //this.deleteDatabase("wine.db"); //Pour effacer l'ancienne database et l'afficher qu'une seule fois
        WDH.dropTable(WDH.getReadableDatabase());
        WDH.populate(); //Create the list of wine

        /*
        Wine[] wines = {
                new Wine("Châteauneuf-du-pape", "vallée du Rhône"),
                new Wine("Arbois", "Jura"),
                new Wine("Beaumes-de-Venise", "vallée du Rhône"),
                new Wine("Begerac", "Sud-ouest"),
                new Wine("Côte-de-Brouilly", "Beaujolais"),
                new Wine("Muscadet", "vallée de la Loire"),
                new Wine("Bandol", "Provence"),
                new Wine("Vouvray", "Indre-et-Loire"),
                new Wine("Ayze", "Savoie"),
                new Wine("Meursault", "Bourgogne"),
                new Wine("Ventoux", "Vallée du Rhône")
        };
        //list de vin
        final WineAdapter adapter = new WineAdapter(this, wines);
        listWine.setAdapter(adapter);
        */


        final Cursor cursor = WDH.fetchAllWines();

        adapter2 = new SimpleCursorAdapter(this, R.layout.wine_adapter, cursor, new String[] {
                WDH.COLUMN_NAME,
                WDH.COLUMN_WINE_REGION,
                WDH.COLUMN_CLIMATE,
                WDH.COLUMN_LOC,
                WDH.COLUMN_PLANTED_AREA },
                new int[] {R.id.Textview_nom, R.id.TextView_region}, 0);

        listWine.setAdapter(adapter2);

        listWine.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                Wine wine = WDH.cursorToWine(cursor);
                wine.setId(position+1);
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("name", wine);
                startActivityForResult(intent,1); //Pour rafraîchir en revenant sur l'écran MainActivity
            }
        });

        //On lie le bouton d'activity_main
        FloatingActionButton fabB = (FloatingActionButton) findViewById(R.id.fab);

        fabB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numberofWine = listWine.getCount(); //Have the number of item in listView and I had 1 to create new ID of wine
                int newID = numberofWine +1;
                Wine wine = new Wine(newID,"Nom", "Region", "Localisation", "Climat", "Surface");
                Intent intent = new Intent(MainActivity.this, AddWineActivity.class);
                intent.putExtra("wineObject", wine);
                startActivityForResult(intent,2);
            }
        });


        //Gestion du ContextMenu

        registerForContextMenu(listWine);
        listWine.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                MainActivity.super.onCreateContextMenu(menu, v, menuInfo);
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.delete, menu);
                //menu.add("Supprimer");
            }
        });
    }




    public static WineDbHelper wdh;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null) { //Si l'intent n'est pas null, sinon l'appli crash en appuyant sur retour arrière
            if(resultCode == 1) { //Si l'intent vient de WineActivity
                Wine wine = data.getParcelableExtra("wineModif");
                WDH.updateWine(wine);

                //Changer le curseur
                String[] all = new String[]{WineDbHelper._ID, WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION, WineDbHelper.COLUMN_LOC, WineDbHelper.COLUMN_CLIMATE, WineDbHelper.COLUMN_PLANTED_AREA};
/*          adapter2.changeCursor(WDH.getReadableDatabase().query(WineDbHelper.TABLE_NAME, all, null, null, null, null, null));

            //Rafraîchir la listView
            adapter2.notifyDataSetChanged();*/

                ListView listWine = (ListView) findViewById(R.id.listWine);
                Cursor cursor = WDH.fetchAllWines();
                adapter2 = new SimpleCursorAdapter(this, R.layout.wine_adapter, cursor, new String[]{
                        WDH.COLUMN_NAME,
                        WDH.COLUMN_WINE_REGION},
                        new int[]{R.id.Textview_nom, R.id.TextView_region}, 0);

                listWine.setAdapter(adapter2);

            }
            else if(resultCode == 2) //Si l'intent vient de AddWineActivity
            {
                Wine wine = data.getParcelableExtra("newWine");
                if(WDH.addWine(wine) == false) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Ajout impossible")
                            .setMessage("Un vin portant le même nom existe déjà dans la base de données.").show();
                }

                //Rafraîchir la listView
                ListView listWine = (ListView) findViewById(R.id.listWine);
                Cursor cursor = WDH.fetchAllWines();
                adapter2 = new SimpleCursorAdapter(this, R.layout.wine_adapter, cursor, new String[]{
                        WDH.COLUMN_NAME,
                        WDH.COLUMN_WINE_REGION},
                        new int[]{R.id.Textview_nom, R.id.TextView_region}, 0);

                listWine.setAdapter(adapter2);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        super.onContextItemSelected(item);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        //if(item.getTitle() == "Supprimer") //Marche pour menu.add
        int index = info.position;
        Cursor cursor = (Cursor) listWine.getItemAtPosition(index);
        WDH.deleteWine(cursor);
        Toast.makeText(this,"Supprimer", Toast.LENGTH_LONG).show();


        cursor = WDH.fetchAllWines();
        adapter2 = new SimpleCursorAdapter(this, R.layout.wine_adapter, cursor, new String[]{
                WDH.COLUMN_NAME,
                WDH.COLUMN_WINE_REGION},
                new int[]{R.id.Textview_nom, R.id.TextView_region}, 0);

        listWine.setAdapter(adapter2);

        return true;
    }
}
