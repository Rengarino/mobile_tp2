package com.example.tp2;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class AddWineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_wine);

        final Wine toBuildWine = (Wine) getIntent().getExtras().get("wineObject");

        //Nom du vin
        final EditText newName = (EditText) findViewById(R.id.addWineName);
        //Région du vin
        final EditText newRegion = (EditText) findViewById(R.id.addWineRegion);
        //Localisation du vin
        final EditText newLoc = (EditText) findViewById(R.id.addWineLoc);
        //Climat
        final EditText newClimat = (EditText) findViewById(R.id.addWineClimate);
        //Surface plantée
        final EditText newSurface = (EditText) findViewById(R.id.addWineSurface);
        //Bouton sauvegarder
        final Button saveB = (Button) findViewById(R.id.buttonAdd);
        saveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ID du vin
                long newID = toBuildWine.getId();

                //Nom du vin
                String newN = newName.getText().toString();

                //Région viticole
                String newR = newRegion.getText().toString();

                //Localisation
                String newL = newLoc.getText().toString();

                //Climat
                String newC = newClimat.getText().toString();

                //Surface plantée
                String newS = newSurface.getText().toString();

                //Créer le nouveau vin et l'update
                Wine newWine = new Wine(newID, newN, newR, newL, newC, newS);

                //Intent
                Intent intentVersMain = new Intent(AddWineActivity.this, MainActivity.class);
                intentVersMain.putExtra("newWine", newWine);
                setResult(2, intentVersMain); //Pour rafraîchir en revenant sur l'écran MainActivity
                finish();
            }
        });
    }
}
