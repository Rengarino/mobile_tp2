package com.example.tp2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class WineActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        //Récupérer l'extra information de l'intent
        final Wine intentWine = (Wine) getIntent().getExtras().get("name");
        //Récupérer le vin à partir de la BDD

        /* Premier test mais faux, à partir d'un intent qui était le nom du vin...
        final String requete = "SELECT * FROM " + WDH.TABLE_NAME + " WHERE" + WDH.COLUMN_NAME +" = " + intentName + " )";
        SQLiteDatabase db = WDH.getReadableDatabase();
        Cursor cursor = db.query(WineDbHelper.TABLE_NAME, new String[] {WineDbHelper._ID, WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION, WineDbHelper.COLUMN_LOC, WineDbHelper.COLUMN_CLIMATE, WineDbHelper.COLUMN_PLANTED_AREA}, WineDbHelper.COLUMN_NAME + " = " + intentName, null, null, null, null);
        Wine wine = WDH.cursorToWine(cursor);
        */

        //Nom du vin
        final EditText wineN = (EditText) findViewById(R.id.wineName);
        wineN.setText(intentWine.getTitle());

        //Région viticole
        final EditText wineR = (EditText) findViewById(R.id.editWineRegion);
        wineR.setText(intentWine.getRegion());

        //Localisation
        final EditText wineL = (EditText) findViewById(R.id.editLoc);
        wineL.setText(intentWine.getLocalization());

        //Climat
        final EditText wineC = (EditText) findViewById(R.id.editClimate);
        wineC.setText(intentWine.getClimate());

        //Surface plantée
        final EditText wineS = (EditText) findViewById(R.id.editPlantedArea);
        wineS.setText(intentWine.getPlantedArea());

        //Bouton sauvegarder
        final Button saveB = (Button) findViewById(R.id.button);
        saveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //ID du vin
                long newID = intentWine.getId();

                //Nom du vin
                String newName = wineN.getText().toString();

                if(newName.isEmpty()) //Si le nom est vide je crée un alertdialog et je return.
                {
                    new AlertDialog.Builder(WineActivity.this)
                            .setTitle("Sauvegarde impossible")
                            .setMessage("Le nom du vin doit être non vide.").show();
                    return;
                }
                //Région viticole
                String newRegion = wineR.getText().toString();

                //Localisation
                String newLoc = wineL.getText().toString();

                //Climat
                String newClimat = wineC.getText().toString();

                //Surface plantée
                String newSurface = wineS.getText().toString();

                //Créer le nouveau vin et l'update
                Wine newWine = new Wine(newID, newName, newRegion, newLoc, newClimat, newSurface);

                //Intent
                Intent intentVersMain = new Intent(WineActivity.this, MainActivity.class);
                intentVersMain.putExtra("wineModif", newWine);
                setResult(1, intentVersMain); //Pour rafraîchir en revenant sur l'écran MainActivity, le 1 = la valeur du startactivityforresult de Main activity
                finish();
            }
        });
    }



}
