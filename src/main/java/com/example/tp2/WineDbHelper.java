package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.SimpleCursorAdapter;

import androidx.appcompat.app.AlertDialog;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //public final String requete = String.format("CREATE TABLE %s (%s INT PRIMARY KEY NOT NULL AUTO_INCREMENT %s TEXT %s TEXT %s TEXT %s TEXT %s numeric);", TABLE_NAME, _ID, COLUMN_NAME, COLUMN_WINE_REGION, COLUMN_LOC, COLUMN_CLIMATE, COLUMN_PLANTED_AREA);

    static final String requete = "CREATE TABLE " + TABLE_NAME + " (" + _ID +" AUTO_INCREMENT," + COLUMN_NAME + " TEXT,"
            + COLUMN_WINE_REGION + " TEXT," + COLUMN_LOC + " TEXT," + COLUMN_CLIMATE + " TEXT," + COLUMN_PLANTED_AREA
            + " TEXT," + " UNIQUE(" + COLUMN_NAME + ", " + COLUMN_WINE_REGION + ") ON CONFLICT ROLLBACK)";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(requete);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion != oldVersion) {
            db.execSQL("drop table if exists " + TABLE_NAME);
            onCreate(db);
        }
    }

    public void dropTable(SQLiteDatabase db) {
        db.execSQL("drop table if exists " + TABLE_NAME);
        onCreate(db);
    }

    /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(_ID, wine.getId());
        cv.put(COLUMN_NAME, wine.getTitle());
        cv.put(COLUMN_WINE_REGION, wine.getRegion());
        cv.put(COLUMN_LOC, wine.getLocalization());
        cv.put(COLUMN_CLIMATE, wine.getClimate());
        cv.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());

        String nom = wine.getTitle();
        String region = wine.getRegion();
        String[] selectionArgs = { nom, region };
        String selection = COLUMN_NAME + " LIKE ? AND " + COLUMN_WINE_REGION + " LIKE ? ";
        final Cursor cursor = db.query(WineDbHelper.TABLE_NAME, new String[] {WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION}, selection, selectionArgs, null, null, null);
        if(cursor == null) //Si le curseur est null alors il n'y a pas de vin qui ressemble à celui que l'on va ajouter dans la table
        {
           return false;
        }

        // Inserting Row

        long rowID = db.insert(TABLE_NAME, null, cv);
        db.close(); // Closing database connection
        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, wine.getTitle());

        values.put(COLUMN_WINE_REGION, wine.getRegion());

        values.put(COLUMN_LOC, wine.getLocalization());

        values.put(COLUMN_CLIMATE, wine.getClimate());

        values.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());

        long id = wine.getId();
        //String selection = _ID + " = ";
        String converted = Long.toString(id);
        String[] selectionArgs = { converted };
        //db.update(TABLE_NAME, values, selection, selectionArgs); Solution alternative
        int res = db.update(TABLE_NAME, values,"_id LIKE ? ", selectionArgs); //Comparaison entre string donc on utilise LIKE, et ? c'est comme %s
        //res returns the number of row affected
        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {
        SQLiteDatabase db = this.getReadableDatabase();

        //call db.query()
        Cursor cursor = db.query(WineDbHelper.TABLE_NAME, new String[] {WineDbHelper._ID, WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION, WineDbHelper.COLUMN_LOC, WineDbHelper.COLUMN_CLIMATE, WineDbHelper.COLUMN_PLANTED_AREA}, null, null, null, null, null);
        //Cursor cursor = db.query(WineDbHelper.TABLE_NAME, new String[] {WineDbHelper._ID, WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION}, null, null, null, null, WineDbHelper.COLUMN_NAME);

        List listWine = new ArrayList<>();

        if (cursor != null) {
            cursor.moveToFirst();
        }

        while(cursor.moveToNext()) {
           String nom = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_NAME));
           String region = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_WINE_REGION));
           listWine.add(nom);
           listWine.add(region);
        }
        return cursor;
    }

    public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        String id = cursor.getString(cursor.getColumnIndex(WineDbHelper._ID));
        String[] selectionArgs = { id };
        db.delete(TABLE_NAME,"_id LIKE ? ", selectionArgs);
        db.close();
    }

    //J'ai rajouter l'id pour modifier en fonction de l'id
    public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine(1,"Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine(2,"Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine(3,"Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine(4,"Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine(5,"Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine(6,"Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine(7,"Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine(8,"Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine(9,"Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine(10,"Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine(11,"Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));



        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        Wine wine = null;
        // build a Wine object from cursor
        String nom = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_NAME));
        String region = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_WINE_REGION));
        String loc = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_LOC));
        String climate = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_CLIMATE));
        String plantedArea = cursor.getString(cursor.getColumnIndex(WineDbHelper.COLUMN_PLANTED_AREA));
        wine = new Wine(nom,  region, loc, climate, plantedArea);
        return wine;
    }
}
