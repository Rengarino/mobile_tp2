package com.example.tp2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class WineAdapter extends ArrayAdapter<Wine>{

    public WineAdapter(Context context, Wine[] wines)
    {
        super(context, 0, wines);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Wine wine = getItem(position);
        if(convertView==null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.wine_adapter, parent, false);
        }
        TextView name = (TextView) convertView.findViewById(R.id.Textview_nom);
        TextView region = (TextView) convertView.findViewById(R.id.TextView_region);

        name.setText(wine.getTitle());
        region.setText(wine.getRegion());
        return convertView;
    }

}
